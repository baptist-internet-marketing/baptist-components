# Baptist Health Toggle CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `Toggle` | Core | The core toggle component |
| `Toggle-input` | Descendent | Core descendent for `<input>` element |
| `Toggle-indicator` | Descendent | The visible toggle element |
| `Toggle--radio` | Modifier | Modifier for radio toggle inputs |

## Configurable variables

- `--Toggle-bg`
- `--Toggle-border-color`
- `--Toggle-border-radius`
- `--Toggle-border-width`
- `--Toggle-color`
- `--Toggle-cursor`
- `--Toggle-focus-shadow`
- `--Toggle-line-height`
- `--Toggle-padding`
- `--Toggle-radio-border-radius`
- `--Toggle-size`
- `--Toggle-transition-duration`
- `--Toggle-transition-timing-function`

## Use

Examples:

```html
<label class="Toggle">
  <input class="Toggle-input" type="checkbox">
  <span class="Toggle-indicator"></span>
  Checkbox
</label>

<label class="Toggle Toggle--radio">
  <input class="Toggle-input" type="radio" value="50">
  <span class="Toggle-indicator"></span>
  Radio
</label>
```

### Theming / extending

The component CSS is focused on core structural requirements for toggles. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-toggle";

/**
 * Extend via configurable variables 
 */

:root {
  --Toggle-border-color: rgb(36, 70, 100);
  --Toggle-border-radius: 0;
  --Toggle-border-width: 5px;
  --Toggle-transition-timing-function: cubic-bezier(1, 0, 0, 1);
}

/**
 * Modifier: Checkbox
 *
 * Uses a checkmark (✓) icon (with the help of `postcss-inline-svg`).
 */

@svg-load --Toggle--checkbox-background-image url("icons/check.svg") {
  fill: var(--Toggle-color);
}

.Toggle--checkbox .Toggle-indicator::after {
  background-color: transparent;
  background-image: svg-inline(--Toggle--checkbox-background-image);
}
```
