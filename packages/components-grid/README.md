# Baptist Health Grid CSS Component

## Installation

TODO

## Available classes

| CSS class           | Type     | Description                                 |
|:--------------------|:---------|:--------------------------------------------|
| `Grid`              | Base     | core component                              |
| `Grid--alignCenter` | Modifier | center-align all child `Grid-cell`          |
| `Grid--alignRight`  | Modifier | right-align all child `Grid-cell`           |
| `Grid--alignMiddle` | Modifier | middle-align all child `Grid-cell`          |
| `Grid--alignBottom` | Modifier | bottom-align all child `Grid-cell`          |
| `Grid--fit`         | Modifier | distribute space of all child `Grid-cell`   |
| `Grid--equalHeight` | Modifier | make each child match height of the tallest |
| `Grid--withGutter`  | Modifier | adds a gutter between cells                 |
| `Grid--withGap`     | Modifier | adds a gap around all sides of cells        |
| `Grid-cell`         | Element  | a child cell of `Grid`                      |
| `Grid-cell--center` | Modifier | center an individual `Grid-cell`            |

## Configurable variables

- `--Grid-gutter-size`
- `--Grid-gap-size`

## Use

Note: The `.Grid` component is intended to be used with separate utility classes to control the width of grid cells.

Examples: See [suitcss/components-grid](https://github.com/suitcss/components-grid) for full examples.

```html
<div class="Grid Grid--alignMiddle">
  <div class="Grid-cell u-size1of4">...</div>
  <div class="Grid-cell u-size3of4">...</div>
</div>
```

### Theming / extending

```css
@import "@baptist-health/components-grid";

/**
 * Extend via configurable variables
 */

:root {
  --Grid-gutter-size: 2vw;
}
```
