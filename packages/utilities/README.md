# Baptist Health Utilities

## Installation

TODO

## Custom media queries

| Selector        | Description                                     |
|:----------------|:------------------------------------------------|
| `--sm-viewport` | Viewports with widths to be considered "small"  |
| `--md-viewport` | Viewports with widths to be considered "medium" |
| `--lg-viewport` | Viewports with widths to be considered "large"  |

## Use

Most of these utility classes inherit directly from (or have been inspired by)
the [SUIT CSS Utils](https://github.com/suitcss/utils) library. They follow the same naming conventions, and also strive to apply styles in a similar low-level fashion.

Examples:

```html
<div class="u-sm-hidden">
  This will be hidden at small viewport widths
</div>

<div class="u-posRelative">
  <div class="u-posAbsoluteBottomRight">
    This will be absolute-positioned to the bottom-right of its parent.
  </div>
</div>
```

### Theming / extending

```css
/* TODO: Update this once we figure it out */
@import "baptist-health-utilities";

/**
 * Specify custom media query values
 */

 @custom-media --sm-viewport (width >= 32em);
 @custom-media --md-viewport (width >= 45em);
 @custom-media --lg-viewport (width >= 62em);
```
