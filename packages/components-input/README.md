# Baptist Health Input CSS Component

## Installation

TODO

## Available classes

| CSS class | Type | Description              |
|-----------|------|--------------------------|
| `Input`   | Core | The core input component |

## Configurable variables

- `--Input-background-color`
- `--Input-border-color`
- `--Input-border-radius`
- `--Input-border-width`
- `--Input-color`
- `--Input-disabled-opacity`
- `--Input-focus-outline`
- `--Input-font-family`
- `--Input-font-size`
- `--Input-font-weight`
- `--Input-line-height`
- `--Input-padding`

## Use

Examples:

```html
<form>
  <input class="Input" type="text">
  <textarea class="Input"></textarea>
</form>
```

### Theming / extending

Using only configurable variables:
```css
/* TODO: Update this once we figure it out */
@import "baptist-health-components-input";

:root {
  --Input-background-color: white;
  --Input-border-color: royalblue;
  --Input-border-radius: 5px;
  --Input-border-width: 2px;
}
```

Adding additional styles:
```css
/* TODO: Update this once we figure it out */
@import "baptist-health-components-input";

:root {
  --Input-background-color: white;
  --Input-border-color: royalblue;
  --Input-border-radius: 5px;
  --Input-border-width: 2px;

  /* New custom properties */
  --Input-enter-color: royalblue;
  --Input-enter-background-color: azure;
}

.Input {
  transition: 200ms ease;
  transition-property: background, color;
}

.Input:focus {
  background-color: var(--Input-enter-background-color);
  color: var(--Input-enter-color);
}
```
