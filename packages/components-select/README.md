# Baptist Health Select CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `Select` | Base  | The core component |

## Configurable variables


- `--Select-background-color`
- `--Select-border-color`
- `--Select-border-radius`
- `--Select-border-width`
- `--Select-caret`
- `--Select-caret-size`
- `--Select-caret-offset`
- `--Select-color`
- `--Select-disabled-opacity`
- `--Select-font-family`
- `--Select-font-size`
- `--Select-font-weight`
- `--Select-line-height`
- `--Select-padding`

## Use

Examples:

```html
<select class="Select">
  <option>Option 1</option>
  <option>Option 2</option>
  <option>Option 3</option>
  <option>Option 4</option>
  <option>Option 5</option>
</select>
```

### Theming / extending

The component CSS is focused on core structural requirements. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-select";

/**
 * Extend via configurable variables 
 */

:root {
  --Select-background-color: black;
  --Select-border-color: white;
  --Select-border-radius: 0;
  --Select-border-width: 1px;
}
```
