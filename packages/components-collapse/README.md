# Baptist Health Collapse CSS Component

## Installation

TODO

## Available classes

| CSS class            | Type    | Description                 |
|:---------------------|:--------|:----------------------------|
| `Collapse`           | Base    | The core component          |
| `Collapse-input`     | Element | The input/toggle            |
| `Collapse-label`     | Element | The input/toggle label      |
| `Collapse-indicator` | Element | The icon indicator          |
| `Collapse-content`   | Element | The content to be collapsed |

## Configurable variables

- `--Collapse-fx-duration`
- `--Collapse-fx-easing`

## Use

Examples:

```html
<div class="Collapse js-Collapse ">
  <input class="Collapse-input"
    type="checkbox"
    id="collapse"
    aria-controls="collapse-content"
    aria-expanded="false">
  <label class="Collapse-label"
    id="collapse-label"
    for="collapse">
    Label Content
    <svg class="Collapse-indicator Icon">
      <!-- ... -->
    </svg>
  </label>
  <div class="Collapse-content"
    id="collapse-content"
    aria-labelledby="collapse-label"
    aria-hidden="true">
    Content...
  </div>
</div>
```

### Theming / extending

Using only configurable variables:
```css
/* TODO: Update this once we figure it out */
@import "baptist-health-components-collapse";

:root {
  --Collapse-fx-duration: 3s;
  --Collapse-fx-easing: linear;
}
```

Adding additional styles:
```css
/* TODO: Update this once we figure it out */
@import "baptist-health-components-collapse";

.Collapse-label {
  font-weight: bold;
}
```
