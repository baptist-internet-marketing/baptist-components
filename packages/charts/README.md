# Baptist Health Chart Themes

## Installation

TODO

## Available classes

See http://www.highcharts.com/docs/chart-design-and-style/style-by-css

## Configurable variables

- `--highcharts-font-family`
- `--highcharts-font-size`
- `--highcharts-heading-font-family`
- `--highcharts-heading-font-weight`
- `--highcharts-font-size-large`
- `--highcharts-font-size-small`
- `--highcharts-tooltip-color`
- `--highcharts-tooltip-background`
- `--highcharts-color-0`
- `--highcharts-color-1`
- `--highcharts-color-2`
- `--highcharts-color-3`
- `--highcharts-color-4`
- `--highcharts-color-5`
- `--highcharts-color-6`
- `--highcharts-color-7`
- `--highcharts-color-8`
- `--highcharts-color-9`

## Use

### Theming / extending

```css
@import "@baptist-health/charts";

/**
 * Extend via configurable variables
 */

:root {
  --highcharts-heading-font-family: cursive;
  --highcharts-heading-font-weight: 900;
}
```
