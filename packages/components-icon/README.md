# Baptist Health Icon CSS Component

## Installation

TODO

## Available classes

| CSS class     | Type     | Description                      |
|---------------|----------|----------------------------------|
| `Icon`        | Core     | The core icon component          |
| `Icon--block` | Modifier | Creates block-level component    |
| `Icon--grow1` | Modifier | Larger than default by one size  |
| `Icon--grow2` | Modifier | Larger than default by two sizes |

## Configurable variables

- `--Icon--grow1-size`
- `--Icon--grow2-size`

## Use

Examples:

```html
<svg width="24" height="24" viewBox="0 0 24 24" class="Icon">
  ...
</svg>

<svg width="24" height="24" viewBox="0 0 24 24" class="Icon Icon--grow1">
  ...
</svg>
```

### Theming / extending

The component CSS is focused on core structural requirements for icons. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-icon";

/**
 * Extend via configurable variables 
 */

:root {
  --Icon--grow1-size: 2.25;
  --Icon--grow2-size: 3;
}
```
