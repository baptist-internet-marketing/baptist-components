# Baptist Health InputExtra CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `InputExtra` | Core  | The core component |
| `InputExtra-input` | Descendent | The component `<input>` element |
| `InputExtra-glyph` | Descendent | The component glyph |

## Configurable variables

- `--InputExtra-glyph-left`
- `--InputExtra-glyph-width`
- `--InputExtra-padding-left`

## Use

Examples:

```html
<div class="InputExtra">
  <input class="InputExtra-input" placeholder="Placeholder text">
  <p class="InputExtra-glyph" aria-hidden="true">$</p>
</div>
```

### Theming / extending

The component CSS is focused on core structural requirements. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-input-extra";

/**
 * Extend via configurable variables 
 */

:root {
  --InputExtra-glyph-left: 0.8em;
  --InputExtra-glyph-width: 1em;
  --InputExtra-padding-left: 2em;
}
```
