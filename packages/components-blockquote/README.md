# Baptist Health Blockquote CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `Blockquote`      | Core  | The core blockquote component       |
| `Blockquote-cite` | Descendent  | The core blockquote cite descendent |

## Configurable variables

- `--Blockquote-cite-font-size`
- `--Blockquote-cite-margin-left`

## Use

Examples:

```html
<blockquote class="Blockquote">
  <p>&ldquo;Hodav zofa ig tadegmo gub ev fud wocre hobbebuf nomkilte duv la bortesdog milcid.&rdquo;</p>
  <footer>
    <cite class="Blockquote-cite">
      Vernon Burton, Ura peradoco behi ameinat ulve nagwa pol loto goace ulubigtah jeva fiughaz.
    </cite>
  </footer>
</blockquote>
```

### Theming / extending

The component CSS is focused on core structural requirements for buttons. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-blockquote";

/**
 * Extend via configurable variables 
 */

:root {
  --Blockquote-cite-font-size: 2em;
  --Blockquote-cite-margin-left: 2em;
}

/** 
 * Extend by adding modifiers 
 */

.Blockquote--bold {
  font-weight: bold;
}
```
