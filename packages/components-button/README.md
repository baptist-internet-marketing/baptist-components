# Baptist Health Button CSS Component

## Installation

TODO

## Available classes

| CSS class     | Type  | Description                      |
|---------------|-------|----------------------------------|
| `Button`      | Core  | The core button component        |
| `is-disabled` | State | For disabled-state button styles |

## Configurable variables

- `--Button-background-color`
- `--Button-border-color`
- `--Button-border-radius`
- `--Button-border-width`
- `--Button-color`
- `--Button-disabled-opacity`
- `--Button-font-family`
- `--Button-font-weight`
- `--Button-line-height`
- `--Button-padding`

## Use

Examples:

```html
<a class="Button" href="...">Next</a>
<a class="Button is-disabled" href="...">Next</a>

<button class="Button" type="button">Close</button>
<button class="Button" type="button" disabled>Edit</button>
```

### Theming / extending

The component CSS is focused on core structural requirements for buttons. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-button";

/**
 * Extend via configurable variables 
 */

:root {
  --Button-background-color: #0179a5;
  --Button-border-color: transparent;
  --Button-border-radius: 12px 3px;
  --Button-color: #fff;
  --Button-font-weight: 600;
  --Button-padding: .5em 1em;
}

/** 
 * Extend by adding modifiers 
 */

.Button--large {
  font-size: 3em;
  padding: 1em 2em;
}
```
