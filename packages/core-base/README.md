# Baptist Health Core Base Styles

## Installation

TODO

## Custom selectors

| Selector  | Description                                         |
|:----------|:----------------------------------------------------|
| `--lists` | All list elements (ul, ol, etc.)                    |
| `--enter` | A shorthand for both hover and focus pseudo-classes |

## Configurable variables

- `--link-color`
- `--link-enter-color`
- `--link-text-decoration`
- `--hr-background-color`
- `--hr-height`
- `--hr-opacity`

## Use

### Theming / extending

These styles are intended to be used in addition to [Normalize.css](https://necolas.github.io/normalize.css/), and should be included immediately after.

```css
/* TODO: Update this once we figure it out */
@import "normalize.css";
@import "baptist-health-core-base";

:root {
  --link-color: royalblue;
  --link-enter-color: midnightblue;
}
```
