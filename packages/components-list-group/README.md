# Baptist Health ListGroup CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `ListGroup` | Core  | The core component |
| `ListGroup-item` | Descendent | The individual items within the component |
| `ListGroup-itemContent` | Descendent | The content within the items |

## Configurable variables

- `--ListGroup-border-color`
- `--ListGroup-border-style`
- `--ListGroup-border-width`
- `--ListGroup-padding-y`

## Use

Examples:

```html
<ul class="ListGroup">
  <li class="ListGroup-item">
    <a class="ListGroup-itemContent" href="/item-1">
      List item 1
    </a>
  </li>
  <li class="ListGroup-item">
    <a class="ListGroup-itemContent" href="/item-2">
      List item 2
    </a>
  </li>
  <li class="ListGroup-item">
    <a class="ListGroup-itemContent" href="/item-3">
      List item 3
    </a>
  </li>
  <li class="ListGroup-item">
    <a class="ListGroup-itemContent" href="/item-4">
      List item 4
    </a>
  </li>
</ul>
```

### Theming / extending

The component CSS is focused on core structural requirements for list groups. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-list-group";

/**
 * Extend via configurable variables 
 */

:root {
  --ListGroup-border-color: #777;
  --ListGroup-border-style: dashed;
  --ListGroup-border-width: 4px;
  --ListGroup-padding-y: 1em;
}
```
