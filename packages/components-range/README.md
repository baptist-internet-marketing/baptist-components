# Baptist Health Range CSS Component

## Installation

TODO

### Dependencies

The following dependencies exist for this CSS component:

- [postcss-mixins](https://github.com/postcss/postcss-mixins)

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `Range` | Base  | The base component |

## Configurable variables

- `--Range-duration-prompt`
- `--Range-thumb-bg`
- `--Range-thumb-border-color`
- `--Range-thumb-border-width`
- `--Range-thumb-size`
- `--Range-thumb-size-md`
- `--Range-track-bg`
- `--Range-track-border-radius`
- `--Range-track-height`

## Use

Examples:

```html
<input class="Range" type="range">
```

### Theming / extending

The component CSS is focused on core structural requirements. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-range";

/**
 * Extend via configurable variables 
 */

:root {
  --Range-duration-prompt: 0.1s;
  --Range-thumb-bg: green;
  --Range-thumb-border-color: yellow;
  --Range-thumb-border-width: 2px;
  --Range-thumb-size: 1.5em;
  --Range-thumb-size-md: 1em;
  --Range-track-bg: green;
}
```
