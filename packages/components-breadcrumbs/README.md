# Baptist Health Breadcrumbs CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `Breadcrumbs` | Core  | The core breadcrumbs component |
| `Breadcrumbs-item` | Descendent | The individual items within the component |
| `Breadcrumbs-link` | Descendent | For links within the component |
| `Breadcrumbs-separator` | Descendent | Optional separator element |

## Configurable variables

- `--Breadcrumbs-gap`
- `--Breadcrumbs-separator-adjust`
- `--Breadcrumbs-separator-size`

## Use

Examples:

```html
<ol class="Breadcrumbs">
  <li class="Breadcrumbs-item">
    <a class="Breadcrumbs-link" href="/">
      Home
    </a>
    <svg class="Breadcrumbs-separator" width="16" height="16" viewBox="0 0 24 24" role="presentation">
      <polyline points="8,5 16,12 8,19" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" fill="none"
      />
    </svg>
  </li>

  <li class="Breadcrumbs-item">
    <a class="Breadcrumbs-link" href="/patient-info">
      Patient Info
    </a>
    <svg class="Breadcrumbs-separator" width="16" height="16" viewBox="0 0 24 24" role="presentation">
      <polyline points="8,5 16,12 8,19" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" fill="none"
      />
    </svg>
  </li>

  <li class="Breadcrumbs-item">
    Classes &amp; Events
  </li>
</ol>
```

### Theming / extending

The component CSS is focused on core structural requirements for breadcrumbs. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-breadcrumbs";

/**
 * Extend via configurable variables 
 */

:root {
  --Breadcrumbs-gap: 1em;
  --Breadcrumbs-separator-size: 1.5em;
}

/** 
 * Extend by adding modifiers 
 */

.Breadcrumbs--bold {
  font-weight: bold;
}
```
