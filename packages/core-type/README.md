# Baptist Health Core Type Styles

## Installation

TODO

## Elements included

- `<body>`
- `<h1>`
- `<h2>`
- `<h3>`
- `<h4>`
- `<h5>`
- `<h6>`

## Custom selectors

| Selector           | Description                                         |
|:-------------------|:----------------------------------------------------|
| `--headings`       | All headings to get common font styling             |
| `--large-headings` | Headings considered "large" for reduced line height |

## Configurable variables

- `--body-color`
- `--body-font-family`
- `--body-font-size`
- `--body-line-height`
- `--heading-font-family`
- `--heading-font-weight`
- `--heading-line-height`

## Use

### Theming / extending

```css
/* TODO: Update this once we figure it out */
@import "baptist-health-core-type";

:root {
  --body-font-family: "proxima-nova", sans-serif;
  --heading-font-family: "proxima-nova-soft", sans-serif;
  --heading-font-weight: 600;
}

@custom-selector :--large-headings h1, h2;

```
