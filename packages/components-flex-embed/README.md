# Baptist Health FlexEmbed CSS Component

## Installation

TODO

## Available classes

| CSS class                | Type     | Description                                                  |
|:-------------------------|:---------|:-------------------------------------------------------------|
| `FlexEmbed`              | Base     | The root node                                                |
| `FlexEmbed-ratio`        | Element  | The element that provides the aspect ratio (1:1 by default)  |
| `FlexEmbed-content`      | Element  | The descendent class for the content that is being displayed |
| `FlexEmbed-ratio--3by1`  | Modifier | The modifier class for 3:1 aspect ratio embed                |
| `FlexEmbed-ratio--2by1`  | Modifier | The modifier class for 2:1 aspect ratio embed                |
| `FlexEmbed-ratio--16by9` | Modifier | The modifier class for 16:9 aspect ratio embed               |
| `FlexEmbed-ratio--4by3`  | Modifier | The modifier class for 4:3 aspect ratio embed                |

## Use

Examples: See [suitcss/components-flex-embed](https://github.com/suitcss/components-flex-embed) for full examples.

```html
<div class="FlexEmbed">
  <div class="FlexEmbed-ratio FlexEmbed-ratio--16by9"></div>
  <div class="FlexEmbed-content">
    <!-- content constrained to a 16:9 aspect ratio -->
    <img src="...">
  </div>
</div>
```

### Theming / extending

```css
@import "@baptist-health/components-flex-embed";

/**
 * New modifier: 3:2 aspect ratio
 */

.FlexEmbed-ratio--3by2 {
  padding-bottom: calc(2/3 * 100%);
}
```
