# Baptist Health Pagination CSS Component

## Installation

TODO

## Available classes

| CSS class                      | Type     | Description                              |
|:-------------------------------|:---------|:-----------------------------------------|
| `Pagination`                   | Base     | The core component                       |
| `Pagination--withPrevious`     | Modifier | Pagination list w/ previous control      |
| `Pagination--withNext`         | Modifier | Pagination list w/ next control          |
| `Pagination-item`              | Element  | The list items within                    |
| `Pagination-link`              | Element  | The links within the list items          |
| `Pagination-control`           | Element  | The previous/next triggers at either end |
| `Pagination-control--previous` | Modifier | Needed to flip the icon                  |
| `Pagination-item--first`       | Modifier | The first list item                      |
| `Pagination-item--last`        | Modifier | The last list item                       |


## Configurable variables

- `--Pagination-caret`
- `--Pagination-control-background-size`
- `--Pagination-control-color-disabled`
- `--Pagination-control-color-enter`
- `--Pagination-control-color`
- `--Pagination-control-width`
- `--Pagination-ellipsis-color`
- `--Pagination-height`
- `--Pagination-line-height`
- `--Pagination-pad`
- `--Pagination-text-decoration`
- `--Pagination-control-opacity-disabled`
- `--Pagination-control-opacity-default`
- `--Pagination-control-opacity-enter`

## Use

Examples:

```html
<nav role="menu" aria-label="Pagination">
  <ul class="Pagination Pagination--withNext">
    <li class="Pagination-item">
      <span class="Pagination-link">
        <span class="u-hiddenVisually">
          Currently viewing page
        </span>
        1
      </span>
    </li>
    <li class="Pagination-item">
      <a class="Pagination-link" href="#">
        <span class="Pagination-control Pagination-control--next">
          <span class="u-hiddenVisually">
            Next:
          </span>
        </span>
        <span class="u-hiddenVisually">
          Page
        </span>
        2
      </a>
    </li>
    <li class="Pagination-item">
      <a class="Pagination-link" href="#">
        <span class="u-hiddenVisually">
          Page
        </span>
        3
      </a>
    </li>
    <li class="Pagination-item">
      <a class="Pagination-link" href="#">
        <span class="u-hiddenVisually">
          Page
        </span>
        4
      </a>
    </li>
  </ul>
</nav>
```

### Theming / extending

The component CSS is focused on core structural requirements. You can extend the core component and build your application-specific theme styles in your app.

```css
@import "@baptist-health/components-pagination";

/**
 * Extend via configurable variables
 */

 :root {
   --Pagination-caret: url("path/to/a/different/icon.svg");
   --Pagination-control-color-enter: red;
   --Pagination-control-color: orange;
   --Pagination-text-decoration: underline;
 }

/**
 * Use different backgrounds for hover/focus.
 */

.Pagination-control:matches(:hover, :focus) {
  background-image: url("path/to/a/different/icon/for/hover.svg");
}
```
