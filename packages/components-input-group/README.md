# Baptist Health InputGroup CSS Component

## Installation

TODO

## Available classes

| CSS class         | Type  | Description                         |
|-------------------|-------|-------------------------------------|
| `InputGroup` | Base  | The base component |
| `InputGroup-cell` | Element | Component child group cell |

## Configurable variables

- `--InputGroup-overlap`

## Use

Examples:

```html
<div class="InputGroup">
  <input class="InputGroup-cell Input" type="text">
  <button class="InputGroup-cell Button">Button</button>
</div>
```

### Theming / extending

The component CSS is focused on core structural requirements. You can extend the core component and build your application-specific theme styles in your app. 

```css
@import "@baptist-health/components-input-group";

/**
 * Extend via configurable variables 
 */

:root {
  --InputGroup-overlap: 2px;
}
```
