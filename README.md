# Baptist Health CSS Components

A set of core CSS components to be extended with modifiers.

## Installation

TODO

## Linting & Building

You may lint the package CSS directly: `./node_modules/.bin/stylelint packages/**/*.css`

There is a general build script: `npm run build`, which uses postcss to output each package to `./build`.

It's expected that projects making use of these components will include the source directly in their own build processes, but these tools can be used to highlight any problems with the CSS.

## Testing

## Browser support
